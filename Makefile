bin/matmul: obj/matriz.o
	gcc -Wall -pthread $^ -o $@
obj/matriz.o: src/matriz.c
	gcc -Wall -pthread -c $^ -o $@


.PHONY: clean
clean:
	rm -rf bin obj
	mkdir bin obj

.PHONY: exe
exe:
	rm stats ; \
	for number in 1 1 1 2 2 2 3 3 3 4 4 4 5 5 5 6 6 6 7 7 7 8 8 8; do \
		echo "# Hilos: "$$number; \
		bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos $$number; \
	done




